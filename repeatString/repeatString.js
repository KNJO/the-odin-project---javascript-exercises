const repeatString = function(str, repeatTimes) {
    if (repeatTimes < 0) return 'ERROR';
    
    let repeatedStr = '';
    for (let i = 0; i < repeatTimes; i++) {
        repeatedStr += str;
    }
    return repeatedStr;
}

module.exports = repeatString
