const palindromes = function(str) {
    let trimmed = str.replace(/[!. ,]+/g, "").toLowerCase();
    let len = trimmed.length;
    for(let i = 0, j = (len - 1); i < (len / 2); i++, j--) {
        if (trimmed[i] != trimmed[j]) return false
    }
    return true;
}

module.exports = palindromes
