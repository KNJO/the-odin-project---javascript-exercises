let findTheOldest = function(people) {
    let oldest = people.reduce((max, curr) => {
        let thisYear = (new Date()).getFullYear();
        let maxAge = (max.yearOfDeath || thisYear) - 
            max.yearOfBirth;
        let currAge = (curr.yearOfDeath || thisYear) - 
            curr.yearOfBirth;
        if (maxAge < currAge) return curr;
        return max;
    }, people[0]);
    return oldest;
}

module.exports = findTheOldest
