const fibonacci = function(num) {
    if (num < 0) return 'OOPS';
    let [first, second] = [1, 1];
    let count = 2;
    while (count < num) {
        let temp = second;
        second += first;
        first = temp;
        count++;
    }
    return second;
}

module.exports = fibonacci
