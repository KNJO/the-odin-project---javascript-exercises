const reverseString = function(str) {
    let arr = str.split('').reverse();
    let reversed = arr.join('');
    // for (let i = 0, j = arr.length - 1; i < j; i++, j--) {
    //     let temp = arr[i];
    //     arr[i] = arr[j];
    //     arr[j] = temp;
    // }
    return reversed;
}

module.exports = reverseString
