const caesar = function(str, shift) {
    
    while (shift > 26) shift -= 26;
    let arr = str.split('');
    
    let shifted = arr.map(letter => {
        let code = letter.charCodeAt();
        if ('A' <= letter && letter <= 'Z') {
            code += shift;
            if (code < 'A'.charCodeAt()) code += 26;
            else if (code > 'Z'.charCodeAt()) code -= 26;
        }
        else if ('a' <= letter && letter <= 'z') {
            code += shift;
            if (code < 'a'.charCodeAt()) code += 26;
            else if (code > 'z'.charCodeAt()) code -= 26;
        }
        return String.fromCharCode(code);
    })

    return shifted.join('')
}

module.exports = caesar
