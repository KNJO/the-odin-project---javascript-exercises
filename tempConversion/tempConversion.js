const ftoc = function(tempInF) {
  let celsius = (tempInF - 32) * (5 / 9);
  let rounded = Math.round(celsius * 10) / 10;
  return rounded;
}

const ctof = function(tempInC) {
  let fahrenheit = tempInC * (9 / 5) + 32;
  let rounded = Math.round(fahrenheit * 10) / 10;
  return rounded;
}

module.exports = {
  ftoc,
  ctof
}
