function add (a, b) {
	return a + b;
}

function subtract (a, b) {
	return a - b;
}

function sum (args) {
	return args.reduce((sum, element) => sum + element, 0)
}

function multiply (args) {
	if (args.length == 0) return 0;
	return args.reduce((product, element) => product * element, 1);
}

function power(base, power) {
	return base ** power;
}

function factorial(num) {
	let fact = 1;
	for (let i = 1 ; i <= num ; fact *= i, i++) {}
	return fact;
}

module.exports = {
	add,
	subtract,
	sum,
	multiply,
    power,
	factorial
}